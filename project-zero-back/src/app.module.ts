import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RestaurantModule } from './restaurant/restaurant.module';
import { UserModule } from './user/user.module';
import { RestaurantPostController } from './restaurant-post/restaurant-post.controller';
import { RestaurantPostModule } from './restaurant-post/restaurant-post.module';
import { AuthModule } from './auth/auth.module';
require('dotenv').config()

@Module({
  imports: [MongooseModule.forRoot(process.env.MONGODB_URI), RestaurantModule, UserModule, RestaurantPostModule, AuthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
