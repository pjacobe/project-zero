import { Test, TestingModule } from '@nestjs/testing';
import { RestaurantPostService } from './restaurant-post.service';

describe('RestaurantPostService', () => {
  let service: RestaurantPostService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RestaurantPostService],
    }).compile();

    service = module.get<RestaurantPostService>(RestaurantPostService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
