import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { ValidateMongoId } from 'src/pipe/isMongoId.pipe';
import { CreateRestaurantPostDTO } from './restaurant-post.dto';
import { RestaurantPostService } from './restaurant-post.service';

@Controller('api/restaurant-post/')
export class RestaurantPostController {

    constructor(private readonly restaurantPostService: RestaurantPostService) { }

    @Post('')
    createPost(@Body() createRestaurantPostDTO: CreateRestaurantPostDTO) {
       return this.restaurantPostService.createPost(createRestaurantPostDTO)
    }

    @Get(':id')
    getRestaurantPostById(@Param('id', ValidateMongoId) id: string) {
        return this.restaurantPostService.getRestaurantPostById(id)
    }

    @Delete(':id')
    deleteRestaurantPostById(@Param('id', ValidateMongoId) id: string) {
        return this.restaurantPostService.deleteRestaurantPostById(id)
    }

}
