import { Test, TestingModule } from '@nestjs/testing';
import { RestaurantPostController } from './restaurant-post.controller';

describe('RestaurantPostController', () => {
  let controller: RestaurantPostController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RestaurantPostController],
    }).compile();

    controller = module.get<RestaurantPostController>(RestaurantPostController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
