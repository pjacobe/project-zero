import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { RestaurantService } from 'src/restaurant/restaurant.service';
import { RestaurantPost, RestaurantPostDocument } from 'src/schema/restaurantPost.schema';
import { CreateRestaurantPostDTO } from './restaurant-post.dto';

@Injectable()
export class RestaurantPostService {
    constructor(@InjectModel(RestaurantPost.name) private restaurantPostModel: Model<RestaurantPostDocument>,
        private readonly restaurantService: RestaurantService) { }

    async createPost(createRestaurantPostDTO: CreateRestaurantPostDTO): Promise<RestaurantPost> {
        try {
            const restaurantPost = await new this.restaurantPostModel(createRestaurantPostDTO).save()
            await this.restaurantService.addRestaurantPost(restaurantPost, createRestaurantPostDTO.poster.toString())
            return restaurantPost
        }
        catch (e) {
            throw new BadRequestException(e.message)
        }
    }

    async getRestaurantPostById(id: string): Promise<RestaurantPost> {
        const restaurantPost = await this.restaurantPostModel.findById(id).populate('poster')

        if (!restaurantPost) {
            throw new NotFoundException()
        }
        return restaurantPost
    }

    async deleteRestaurantPostById(id: string) : Promise<RestaurantPost> 
    {
        const restaurantPost = await this.restaurantPostModel.findByIdAndDelete(id)

        if (!restaurantPost) {
            throw new NotFoundException()
        }
        return restaurantPost
    }
}
