import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RestaurantModule } from 'src/restaurant/restaurant.module';
import { RestaurantPost, RestaurantPostSchema } from 'src/schema/restaurantPost.schema';
import { RestaurantPostController } from './restaurant-post.controller';
import { RestaurantPostService } from './restaurant-post.service';

@Module({
  imports: [MongooseModule.forFeature([{name: RestaurantPost.name, schema:RestaurantPostSchema}]), RestaurantModule],
  controllers: [RestaurantPostController],
  providers: [RestaurantPostService],
  exports: [RestaurantPostService]
})
export class RestaurantPostModule {}
