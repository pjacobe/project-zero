import * as mongoose from "mongoose"
import { IsMongoId, IsNotEmpty } from "class-validator"
import { Type } from "class-transformer"

export class CreateRestaurantPostDTO {
    @IsMongoId()
    poster: mongoose.Schema.Types.ObjectId

    @IsNotEmpty()
    title: string
    @IsNotEmpty()
    body: string
}