import { Body, Controller, Get, Param, Post, Put, Req, Request, UseGuards } from '@nestjs/common';
import { AccessTokenGuard } from 'src/auth/accessToken.guard';
import { CreateUserDTO } from './user.dto';
import { UserService } from './user.service';

@Controller('api/user')
export class UserController {
    constructor(private readonly userService: UserService) {

    }

    @Post("/create")
    async addUser(@Body() createUserDTO: CreateUserDTO) {
        return this.userService.addUser(createUserDTO)
    }

    @UseGuards(AccessTokenGuard)
    @Get("/profile")
    async getProfile(@Request() req)
    {
        return req.user
    }

    @Get("/:id")
    async getUser(@Param('id') id: string) {
        return this.userService.getUser(id)
    }

    @Put("/follow/:id")
    async userFollowRestaurant(@Body("restaurantId") restaurantId: string, @Param('id') userId: string) {
        return this.userService.userFollowRestaurant(userId, restaurantId)
    }

    

}
