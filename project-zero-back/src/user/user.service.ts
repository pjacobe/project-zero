import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { RestaurantService } from 'src/restaurant/restaurant.service';
import { User, UserDocument } from 'src/schema/user.schema';
import { CreateUserDTO, UpdateUserDTO } from './user.dto';

@Injectable()
export class UserService {
    constructor(@InjectModel(User.name) private userModel: Model<UserDocument>, private readonly restaurantService: RestaurantService) { }

    async addUser(createUserDTO: CreateUserDTO): Promise<UserDocument> {
        try {
            const user = await new this.userModel(createUserDTO)
            return await user.save()
        }
        catch (e) {
            throw new BadRequestException(e.message)
        }

    }

    async getUser(id: string): Promise<UserDocument> {
        const user = await this.userModel.findById(id)

        if (!user) {
            throw new BadRequestException("User not found")
        }
        return user
    }

    async userFollowRestaurant(userId: string, restaurantId: string): Promise<User> {
        const restaurant = await this.restaurantService.getRestaurant(restaurantId)

        const user = await this.userModel.findByIdAndUpdate(userId, { $addToSet: { following: restaurant } }, { new: true })

        await this.restaurantService.addFollower(user, restaurantId)

        return user
    }

    async getUserByEmail(email: string): Promise<User> {
        const user = await this.userModel.findOne({ email }).select('+password')
        return user
    }

    async getUserByIdAndUpdate(updateUserDTO: UpdateUserDTO, id: string) : Promise<UserDocument> {
        return this.userModel
        .findByIdAndUpdate(id, updateUserDTO, { new: true })
    }
}
