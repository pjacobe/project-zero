import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from 'src/schema/user.schema';
import { Restaurant } from 'src/schema/restaurant.schema';
import { RestaurantModule } from 'src/restaurant/restaurant.module';

@Module({
  imports:[MongooseModule.forFeature([{name: User.name, schema: UserSchema}]), RestaurantModule],
  providers: [UserService],
  controllers: [UserController],
  exports:[UserService]
})
export class UserModule {}
