import { PartialType } from "@nestjs/mapped-types"

export class CreateUserDTO {
    firstname:string
    lastname:string
    email:string
    city:string
    password:string
    refreshToken:string
}

export class UpdateUserDTO extends PartialType(CreateUserDTO){}