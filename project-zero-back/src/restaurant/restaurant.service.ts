
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { Restaurant, RestaurantDocument } from "src/schema/restaurant.schema";
import { CreateRestaurantDTO } from "./restaurant.dto";
import { User } from "src/schema/user.schema";
import { RestaurantPost } from "src/schema/restaurantPost.schema";

@Injectable()
export class RestaurantService {
    constructor(@InjectModel(Restaurant.name) private restaurantModel: Model<RestaurantDocument>) {

    }

    async createRestaurant(createRestaurantDTO: CreateRestaurantDTO) {
        try {
            const restaurant = await new this.restaurantModel(createRestaurantDTO)
            return await restaurant.save()
        }
        catch (e) {
            throw new BadRequestException(e.message)
        }
    }

    async getAll() {
        const restaurants = await this.restaurantModel.find().select('-password')
        return restaurants
    }

    async getRestaurant(id: string) {
        const restaurant = await this.restaurantModel.findById(id).select('-password')
        return restaurant
    }

    async deleteRestaurant(id: string) {
        const restaurant = await this.restaurantModel.findByIdAndDelete(id).select('-password')
        return restaurant
    }

    async updateRestaurant(resume: string, id: string) {
        const restaurant = await this.restaurantModel.findByIdAndUpdate(id, { resume }, { new: true })
        return restaurant
    }

    async addFollower(user: User, id: string) {
        const restaurant = await this.restaurantModel.findByIdAndUpdate(id, { $addToSet: { followers: user } }, { new: true })
    }

    async addRestaurantPost(restaurantPost: RestaurantPost, id: string) {
        const restaurant = await this.restaurantModel.findByIdAndUpdate(id, { $addToSet: { posts: restaurantPost } }, { new: true })

        if (!restaurant)
            throw new NotFoundException("Restaurant not found")
    }

    async getRestaurantByEmail(email: string): Promise<Restaurant> {
        const restaurant = await this.restaurantModel.findOne({ email })
        return restaurant
    }
}
