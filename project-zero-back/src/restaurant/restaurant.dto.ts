export class CreateRestaurantDTO {
    restaurantName: string
    email: string
    password: string
    siret: number
    address: string
    postalCode: number
    city: string
    country: string
    type: [string]
    resume: string
}