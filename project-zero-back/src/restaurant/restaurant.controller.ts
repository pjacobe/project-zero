import { Body, Controller, Delete, Get, Param, Patch, Post, Put } from '@nestjs/common';
import { CreateRestaurantDTO } from './restaurant.dto';
import { RestaurantService } from './restaurant.service';

@Controller('')
export class RestaurantController {

    constructor(private readonly restaurantService: RestaurantService) { }

    @Post("/api/restaurant")
    createRestaurant(@Body() createRestaurantDTO: CreateRestaurantDTO) {
        return this.restaurantService.createRestaurant(createRestaurantDTO)
    }

    @Get("/api/restaurant/getAll")
    getAll() {
        return this.restaurantService.getAll()
    }

    @Get("/api/restaurant/:id")
    getRestaurant(@Param('id') id: string) {
        return this.restaurantService.getRestaurant(id)
    }

    @Delete("api/restaurant/:id")
    deleteRestaurant(@Param('id') id: string) {
        return this.restaurantService.deleteRestaurant(id)
    }

    @Patch("api/restaurant/:id")
    updateRestaurant(@Body("resume") resume: string, @Param('id') id: string) {
        return this.restaurantService.updateRestaurant(resume, id)
    }

}
