import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from "@nestjs/common";
import { Types } from "mongoose";


export class ValidateMongoId implements PipeTransform<string> {
    transform(value: string, metadata: ArgumentMetadata): string {
        if (Types.ObjectId.isValid(value)) {
            if ((String)(new Types.ObjectId(value)) === value)
                return value;
            throw new BadRequestException
        }
        throw new BadRequestException

    };
}