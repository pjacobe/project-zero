import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose"
import {Document} from "mongoose"
import * as mongoose from "mongoose";
import isEmail from 'validator/lib/isEmail'
import { User } from "./user.schema";
import { RestaurantPost } from "./restaurantPost.schema";

export type RestaurantDocument = Restaurant & Document

@Schema()
export class Restaurant {
    @Prop({ required: true, maxlength: 20 })
    restaurantName: string

    @Prop({ required: true, maxlength: 30, validate: { validator: isEmail, message: "Wrong email format" } })
    email: string

    @Prop({ required: true, minlength: 7, maxlength: 1000, select:false })
    password: string

    @Prop({ required: true, validate:{validator: function(val){
        return val.toString().length === 14
    }, message: val => `${val.value} has to be 14 digits`}})
    siret: number

    @Prop({ required: true, maxlength: 50 })
    address: string

    @Prop({ required: true, length: 5 })
    postalCode: number

    @Prop({ required: true, maxlength: 30 })
    city: string

    @Prop({ required: true, maxlength: 20 })
    country: string

    @Prop({ required: true })
    type: [string]

    @Prop({ required: true, minlength: 10, maxlength: 2048 })
    resume: string

    @Prop([{type: mongoose.Schema.Types.ObjectId, ref: User.name }])
    followers: User[]

    @Prop([{type: mongoose.Schema.Types.ObjectId, ref: Restaurant.name}])
    posts: RestaurantPost[]
}

export const RestaurantSchema = SchemaFactory.createForClass(Restaurant)