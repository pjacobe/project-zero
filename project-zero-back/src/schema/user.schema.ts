import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose"
import * as mongoose from "mongoose";
import { Document } from "mongoose"
import * as argon2 from 'argon2';
import { genSalt, hash } from "bcrypt";
import isEmail from 'validator/lib/isEmail'
import { Restaurant } from "./restaurant.schema";

export type UserDocument = Document & User

@Schema()
export class User {
    @Prop({ required: true, maxlength: 20 })
    firstname: string

    @Prop({ required: true, maxlength: 20 })
    lastname: string

    @Prop({ required: true, maxlength: 30, validate: { validator: isEmail, message: "Wrong email format" } })
    email: string

    @Prop({ required: true, maxlength: 20 })
    city: string

    @Prop({ required: true, minlength: 10, maxlength: 1024, select: false })
    password: string

    @Prop({ type: [mongoose.Schema.Types.ObjectId] })
    following: Restaurant[]

    @Prop({})
    refreshToken: string
}

export const UserSchema = SchemaFactory.createForClass(User)

UserSchema.pre('save', async function (next) {
    if (this.isModified('password')) {
        const salt = await genSalt(10)
        this.password = await hash(this.password, salt)
    }
    next()
})

UserSchema.pre('findOneAndUpdate', async function name(next: any) {
    const modifiedField : any = this.getUpdate()
    modifiedField.refreshToken = await argon2.hash(modifiedField.refreshToken)
    next()
})
