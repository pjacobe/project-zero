import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose"
import {Document, Types} from "mongoose"

import * as mongoose from "mongoose"
import { Restaurant } from "./restaurant.schema"



export type RestaurantPostDocument = RestaurantPost & Document

@Schema()
export class RestaurantPost {

    @Prop({type: mongoose.Schema.Types.ObjectId, required:true, ref:Restaurant.name})
    poster: mongoose.Schema.Types.ObjectId

    @Prop({required:true, maxlength: 30})
    title: string

    @Prop({required:true, maxlength:1024})
    body: string

    @Prop([{type:String}])
    files:string[]
    
}

export const RestaurantPostSchema = SchemaFactory.createForClass(RestaurantPost)