import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { HttpException, Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';
import { User } from 'src/schema/user.schema';
import { Request } from 'express';
import { Restaurant } from 'src/schema/restaurant.schema';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private authService: AuthService) {
        super({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        });
    }

    async validate(req: Request, email: string, password: string): Promise<any> {
        console.log(password)

        let userRestaurant: Restaurant | User = null

        if (req.body.type === 'user') {
            userRestaurant = await this.authService.validateUser(email, password);
        }
        else {
            userRestaurant = await this.authService.validateRestaurant(email, password);
        }

        if (!userRestaurant) {
            throw new UnauthorizedException("Wrong password or email");
        }
        return userRestaurant;
    }
}
