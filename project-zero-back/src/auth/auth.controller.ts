import { Body, Controller, Get, Post, Req, Request, Res, UseGuards } from '@nestjs/common';
import { Response } from 'express';

import { CreateUserDTO } from 'src/user/user.dto';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './local-auth.guards';
import { RefreshTokenGuard } from './refreshToken.guard';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {

    }

    @Post('/signup')
    async signup(@Body() createUserDTO: CreateUserDTO)
    {
        const tokens = await this.authService.signup(createUserDTO)
        return tokens
    }

    @Get('cookie')
    async getCookie(@Req() req)
    {
        console.log(req.signedCookies.jwt)
        return "ok"
    }

    @UseGuards(RefreshTokenGuard)
    @Get('refreshToken')
    async getRefreshToken(@Req() req)
    {
        const {accessToken, refreshToken} = await this.authService.refreshTokens(req.user.sub, req.user.refreshToken)
        return {accessToken, refreshToken}
    }

    @UseGuards(LocalAuthGuard)
    @Post('/login')
    async login(@Req() req, @Res() res : Response) {
        const {accessToken, refreshToken} = await this.authService.login(req.user);
        res.cookie('jwt', refreshToken, {httpOnly:true, signed:true, maxAge: 60*1000*60*24*30, secure:false})
        return res.send({accessToken})
    }

}
