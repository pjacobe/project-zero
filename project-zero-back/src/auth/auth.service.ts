import { ForbiddenException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/user/user.service';
import * as bcrypt from 'bcrypt';
import * as argon2 from 'argon2'
import { RestaurantService } from 'src/restaurant/restaurant.service';
import { jwtConstants } from './const';
import { CreateUserDTO } from 'src/user/user.dto';

@Injectable()
export class AuthService {
    constructor(private usersService: UserService, private restaurantService: RestaurantService, private jwtService: JwtService) { }

    async validateUser(email: string, password: string): Promise<any> {

        const user = await this.usersService.getUserByEmail(email);
        if (user) {
            const isMatch = await bcrypt.compare(password, user.password)
            if (isMatch) {
                return user
            }
            return null;
        }
        return null;
    }

    async validateRestaurant(email: string, password: string): Promise<any> {

        const restaurant = await this.restaurantService.getRestaurantByEmail(email);
        if (restaurant) {
            const isMatch = await bcrypt.compare(password, restaurant.password)
            if (isMatch) {
                return restaurant
            }
            return null;
        }
        return null;
    }

    async getTokens(userId: string, email: string) {
        const [accessToken, refreshToken] = await Promise.all([
            this.jwtService.signAsync(
                {
                    sub: userId,
                    email,
                },
                {
                    secret: jwtConstants.JWT_ACCESS_SECRET,
                    expiresIn: '15m',
                },
            ),
            this.jwtService.signAsync(
                {
                    sub: userId,
                    email,
                },
                {
                    secret: jwtConstants.JWT_REFRESH_SECRET,
                    expiresIn: '7d',
                },
            ),
        ]);

        return {
            accessToken,
            refreshToken,
        };
    }

    async login(user: any) {
        //const payload = { username: user.username, sub: user._id };
        //return {
          //  access_token: this.jwtService.sign(payload),
        //};
        const tokens = await this.getTokens(user._id, user.email)
        const userUpdate = await this.usersService.getUserByIdAndUpdate({refreshToken: tokens.refreshToken}, user._id)
        return tokens
    }

    async signup(createUserDTO: CreateUserDTO)
    {
        const user = await this.usersService.addUser(createUserDTO)
        const tokens = await this.getTokens(user._id, user.email)

        await this.usersService.getUserByIdAndUpdate({refreshToken: tokens.refreshToken}, user._id)
        return tokens

    }

    async refreshTokens(userId: string, refreshToken: string) {
        const user = await this.usersService.getUser(userId);
        if (!user || !user.refreshToken)
          throw new ForbiddenException('Access Denied 1');
        const refreshTokenMatches = await argon2.verify(user.refreshToken, refreshToken)
        console.log(refreshTokenMatches)
        if (!refreshTokenMatches) throw new ForbiddenException('Access Denied 2');
        const tokens = await this.getTokens(user._id, user.email);
        await this.usersService.getUserByIdAndUpdate({refreshToken: tokens.refreshToken}, user._id)
        return tokens;
      }
}
